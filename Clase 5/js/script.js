var info;
$(document).ready(function(){
	$('#contenedorLogin').addClass('animated bounceInDown');
	$('#formularioLogin').on("submit", formularioEnvio);
	$('#salir').on("click", salirBoton);
})
function salirBoton(){
	$('#juego').animate({opacity: 0}, 1000, function(){
		$('#contenedorLogin').removeClass("bounceInDown bounceOutUp");
		$('#contenedorLogin').addClass("bounceInDown");	
		$('#formularioLogin').trigger("reset");
	});
	return false;
}
function formularioEnvio(){
	info = $('#formularioLogin').serializeJSON();
	$.ajax({
		url: 'js/usuarios.json',
		type: 'get',
		error: function(error){ alert("error")},
		success: function(data){ 
			var login = data.filter(function(objeto){
				return objeto.usuario == info.usuario && objeto.password == info.contrasena;
			})
			if (login.length>0) {
				$('#contenedorLogin').addClass("bounceOutUp");
				$('#juego').animate({opacity: 1}, 2000);
				$('#juego h2 span').html(login[0].usuario);
			}else{
				$('.alert').addClass('show');
			}
		}
	});
	return false;
}