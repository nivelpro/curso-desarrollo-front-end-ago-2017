import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  providers: [NgbCarouselConfig]
})

export class SliderComponent implements OnInit {

  constructor(configuracion:NgbCarouselConfig) {
  	configuracion.interval=10000;
  	configuracion.keyboard=true;
  }

  ngOnInit() {
  	
  }



}
