var numero1 = "";
var numero2 = "";
var flagOperaciones;
var operacion = "";
var resultado = 0;

$(document).ready(function() {
	$('.numeros').on("click", clickNumero);
	$('.operaciones').on("click", clickOperaciones);
	console.log("ok");
})

function clickNumero() {
	var numero = $(this).html();
	if (numero!="C") {
		if (flagOperaciones) {
			$('#resultado').html('');
			flagOperaciones = false;
		}
		if ($('#resultado').html()==0) {
			$('#resultado').html('');	
		}
		$('#resultado').append(numero)	
	}else{
		numero1 = "";
		numero2 = "";
		flagOperaciones = false;
		operacion = "";
		resultado = 0;
		$('#resultado').html('0');
	}
}

function clickOperaciones() {
	if (numero1.length > 0) {
		numero2 = $('#resultado').html();	
	}else{
		numero1 = $('#resultado').html();
	}
	flagOperaciones = true;
	if ($(this).html() != "=") {
		operacion = $(this).html();
	}else{
		calcular();
		$('#resultado').html(resultado);
	}
}
function calcular(){
	switch(operacion){
		case '+':
			resultado = parseInt(numero1)+parseInt(numero2);
			break;
		case '-':
			resultado = parseInt(numero1)-parseInt(numero2);
		case 'X':
			resultado = parseInt(numero1)*parseInt(numero2);
			break;
		case '%':
			resultado = parseInt(numero1)/parseInt(numero2);
	}
	numero1="";
	numero2="";
	operacion="";
}

