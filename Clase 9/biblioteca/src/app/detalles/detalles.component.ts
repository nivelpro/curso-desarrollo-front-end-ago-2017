import { Component, OnInit } from '@angular/core';
import {LibrosseleccionadosService} from '../librosseleccionados/librosseleccionados.service';
import { ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.component.html',
  styleUrls: ['./detalles.component.css']
})
export class DetallesComponent implements OnInit {

	libros:Array<Object>;
	libroSeleccionado: Object;
	libroId:Number;

	constructor(private miservicio:LibrosseleccionadosService, private ruta:ActivatedRoute) { 
		this.libros=[];
		this.libroSeleccionado={};
		miservicio.cargarLibrosJSON().subscribe(
			datosServicio=>{
				this.libros=datosServicio;
				this.ejecutarBusqueda();
			}
		);
	}

	ngOnInit() {
	}
	ejecutarBusqueda(){
		this.ruta.params.subscribe(
			params=>{
				this.libroId = params["libroId"];
				this.libroSeleccionado = this.libros.filter(this.filtroPorID, this.libroId)[0];
				console.log(this.libroSeleccionado);
			}
		)
	}

	filtroPorID(libro){
		return libro.id==this;
	}
	agregarFavorito(milibro){
		this.miservicio.agregarLibros(milibro);

	}

}
