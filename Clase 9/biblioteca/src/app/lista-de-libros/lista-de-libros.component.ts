import { Component, OnInit } from '@angular/core';
import {LibrosseleccionadosService} from '../librosseleccionados/librosseleccionados.service';

@Component({
  selector: 'app-lista-de-libros',
  templateUrl: './lista-de-libros.component.html',
  styleUrls: ['./lista-de-libros.component.css']
})
export class ListaDeLibrosComponent implements OnInit {

	cargando:Boolean;
	errorHttp:Boolean;
	libros:Array<Object>;

  	constructor(private miservicio:LibrosseleccionadosService) { }

	ngOnInit() {
	  	this.cargando = true;
	  	this.miservicio.cargarLibrosJSON().subscribe(
	  		datosServicio=>{
	  			this.libros = datosServicio;
	  			this.cargando = false;
	  			console.log(this.libros);
	  		}
	  	)
	}

}
