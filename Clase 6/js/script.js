var info;
var fichasContador = 0;
var marcadores = {tiempo:30,puntaje:0}
var arrayClases = ["ficha1", "ficha2", "ficha3", "ficha4", "ficha5"];
$(document).ready(function(){
	$('#contenedorLogin').addClass('animated bounceInDown');
	$('#formularioLogin').on("submit", formularioEnvio);
	$('#salir').on("click", salirBoton);
	$('#iniciar').on("click", iniciarJuego);
	$('#reintentar').on("click", reiniciar);
})

function contadorJuego(){
	var tiempo = setInterval(function(){
		marcadores.tiempo--;
		$('#numero2').html(marcadores.tiempo);
		if (marcadores.tiempo==0) {
			$('#finJuego').modal('show');
			$('#puntajeFinal').html(marcadores.puntaje);
			clearInterval(tiempo);
		}
	}, 1000);
}
function reiniciar(){
	$('#finJuego').modal('hide');
	marcadores = {tiempo:30,puntaje:0}
	fichasContador= 0;
	$('#contenedorJuego').html("");
	loopJuego();
	contadorJuego();
}
function iniciarJuego(){
	$('#juego article').slideUp("slow");
	$('#contenedorJuego').slideDown("slow");
	$('#marcadores').slideDown("slow");
	loopJuego();
	contadorJuego();
}
function clickFicha(){
	marcadores.puntaje++;
	$(this).remove();
	$('#numero1').html(marcadores.puntaje);
}
function loopJuego(){
	if(marcadores.tiempo >0){
		var randomTime = Math.random()*500*2;
		setTimeout(function(){
			fichasContador++;
			adicionarFicha();
			loopJuego();
		}, randomTime);
	}else{return false;}
}
//var arrayClases = ["ficha1", "ficha2", "ficha3", "ficha4", "ficha5"];
function adicionarFicha(){
	var claseFicha = arrayClases[Math.floor(Math.random()*arrayClases.length)]
	var ficha = $('<div class="ficha '+claseFicha+'"></div>');
	$('#contenedorJuego').prepend(ficha);
	console.log($('#contenedorJuego').width());
	var fichaX = Math.abs((Math.random()*$('#contenedorJuego').width())-100);
	ficha.css({'left': fichaX+'px'});
	
	ficha.animate({
		top: "600px"
	}, 4000, function(){
		$(this).remove();
	});
	ficha.on("click", clickFicha);
}

function salirBoton(){
	$('#juego').animate({opacity: 0}, 1000, function(){
		$('#contenedorLogin').removeClass("bounceInDown bounceOutUp");
		$('#contenedorLogin').addClass("bounceInDown");	
		$('#formularioLogin').trigger("reset");
	});
	return false;
}
function formularioEnvio(){
	info = $('#formularioLogin').serializeJSON();
	$.ajax({
		url: 'js/usuarios.json',
		type: 'get',
		error: function(error){ alert("error")},
		success: function(data){ 
			var login = data.filter(function(objeto){
				return objeto.usuario == info.usuario && objeto.password == info.contrasena;
			})
			if (login.length>0) {
				$('#contenedorLogin').addClass("bounceOutUp");
				$('#juego').animate({opacity: 1}, 2000);
				$('#juego h2 span').html(login[0].usuario);
			}else{
				$('.alert').addClass('show');
			}
		}
	});
	return false;
}