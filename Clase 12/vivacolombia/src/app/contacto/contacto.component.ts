import { Component, OnInit } from '@angular/core';

declare var emailjs:any;

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {

	usuario={
		nombre:"",
		email:"",
		telefono:"",
		mensaje:""
	}

  constructor() { }

  ngOnInit() {
  }

  enviarCorreo(usuario){
  	var datos={
  		to_name : "Giovanny",
  		from_name : usuario.nombre,
  		message_html : "email: "+usuario.email+"<br>telefono: "+usuario.telefono+"<br>mensaje: "+usuario.mensaje
  	};
  	emailjs.init("user_8hbE9sktIf8kipARLtAot");
  	emailjs.send("gmail", "template_5xDF1VhS", datos).then(
  		function(respuesta){
  			console.log("ok");
  			alert("Su correo fue enviado");
  		},
  		function(error){
  			console.log(error);
  			alert("Error al enviar el correo");
  		}
  	)

  	console.log(usuario);
  }

}
