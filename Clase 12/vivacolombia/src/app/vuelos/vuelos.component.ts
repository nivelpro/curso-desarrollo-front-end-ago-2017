import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-vuelos',
  templateUrl: './vuelos.component.html',
  styleUrls: ['./vuelos.component.scss']
})
export class VuelosComponent implements OnInit {

	ciudades:Array<Object>;
	antiguo:HTMLElement;
	ciudadesSeleccionadas:number;

	constructor(private renderer:Renderer2) {
		this.ciudades=[
			{nombre: "Cali", id:1},
			{nombre: "Medellín", id:2},
			{nombre: "Bogotá", id:3},
			{nombre: "Cartagena", id:4},
			{nombre: "Santa Marta", id:5},
			{nombre: "Villavicencio", id:6}
		];
		this.ciudadesSeleccionadas = 0;
	}

  	ngOnInit() {
  	}

  	mostrarActivo(elemento:HTMLElement, boton:HTMLElement){
  		if (this.antiguo) {
  			this.renderer.removeClass(this.antiguo, 'active');
  		}



  		this.renderer.addClass(elemento, 'active');

  		//<i class="fa fa-check-square-o" aria-hidden="true"></i>

  		let icono = this.renderer.createElement("span");
  		this.renderer.setProperty(icono, "innerHTML", '<i class="fa fa-check-square-o" aria-hidden="true"></i>');
  		this.renderer.appendChild(elemento, icono);
  		this.antiguo=elemento;

  		this.ciudadesSeleccionadas++;

  		if (this.ciudadesSeleccionadas>=3) {
  			this.renderer.setProperty(boton, "innerHTML", "A viajar =>");
  			this.renderer.removeAttribute(boton, "disabled");
  		}


  	}

}
