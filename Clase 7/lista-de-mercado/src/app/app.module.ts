import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { PieDePaginaComponent } from './pie-de-pagina/pie-de-pagina.component';
import { ComprasComponent } from './compras/compras.component';
import { CompraIndividualComponent } from './compra-individual/compra-individual.component';
import { CrearCompraComponent } from './crear-compra/crear-compra.component';

//Importar Formularios de Angular 4
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    PieDePaginaComponent,
    ComprasComponent,
    CompraIndividualComponent,
    CrearCompraComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
