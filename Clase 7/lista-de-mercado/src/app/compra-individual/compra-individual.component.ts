import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-compra-individual',
  templateUrl: './compra-individual.component.html',
  styleUrls: ['./compra-individual.component.css']
})
export class CompraIndividualComponent implements OnInit {

	@Input()
	compraInfo:any;

  constructor() { }

  ngOnInit() {
  }

  public articuloComprado(compra){
  	console.log(compra);
  	compra.comprado = true;
  }

}
