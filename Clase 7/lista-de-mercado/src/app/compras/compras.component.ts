import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})
export class ComprasComponent implements OnInit {

  constructor() { }

  compras: Array<Object>;
  error: any;

  ngOnInit() {
    this.compras = JSON.parse(localStorage.compras);
  	console.log(this.compras);
  }
}
