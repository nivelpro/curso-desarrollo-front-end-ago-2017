import { Component, OnInit } from '@angular/core';




@Component({
  selector: 'app-crear-compra',
  templateUrl: './crear-compra.component.html',
  styleUrls: ['./crear-compra.component.css']
})
export class CrearCompraComponent implements OnInit {

	nuevaCompra={
		texto:'',
		valor:0,
		comprado: false	
	}
	comprasTotal:Array<Object>;
	constructor() { }

	ngOnInit() {
		this.comprasTotal=JSON.parse(localStorage.compras);
	}

	public guardarCompra(nuevo){
		console.log(nuevo);
		this.comprasTotal.push(nuevo);
		localStorage.setItem("compras", JSON.stringify(this.comprasTotal));
	}

}
