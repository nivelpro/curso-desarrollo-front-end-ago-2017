import { Component, OnInit } from '@angular/core';

import {NgbTooltipConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [NgbTooltipConfig]
})
export class DashboardComponent implements OnInit {
	constructor(configuracion:NgbTooltipConfig) {
		configuracion.placement='bottom';
	}
  graficaData:Array<any>
  graficaLabels:Array<any>
  graficaChartType:string
  graficaOptions:any;
  graficaColors:Array<any>
  graficaLegend:boolean

  infoData:number[]
  infoDataRadar:Array<any>
  infoLabels:string[]
  infoChartType:string
  infoChartTypeRadar:string
  
  ngOnInit() {
  	this.graficaData = [
  		{
  			data:[100000, 150000, 50000, 250000, 1000000, 0, 100000],
  			label: "2017"
  		},
  		{
  			data:[200000, 180000, 90000, 1250000, 1040000, 100000, 400000],
  			label: "2016"
  		},
  		{
  			data:[60000, 6150000, 450000, 150000, 500000, 250000, 130000],
  			label: "2015"
  		}]
  	this.graficaLabels = ["Ene", "Feb", "Mar","Abr", "May", "Jun", "Jul"]
  	this.graficaChartType = 'line'
  	//this.graficaChartType = 'bar'
  	this.graficaOptions = {
  		showLines:true,
  		animation: {
            duration: 5000, // general animation time
        }}
  	this.graficaColors=[
  		{
  			backgroundColor:"rgba(148,160,190,0.5)",
  			borderColor:"rgba(148,159,177,1)",
  			pointBackgroundColor:"#7c1d1d",
  			pointBorderColor:"#7c1d1d"

  		},
  		{
  			backgroundColor:"rgba(193, 126, 25,0.5)",
  			borderColor:"rgba(193, 126, 25,0.5)",
  			pointBackgroundColor:"#7c1d1d",
  			pointBorderColor:"#7c1d1d"

  		},
  		{
  			backgroundColor:"rgba(30, 137, 73,0.5)",
  			borderColor:"rgba(30, 137, 73,0.5)",
  			pointBackgroundColor:"#7c1d1d",
  			pointBorderColor:"#7c1d1d"

  		}]
  	this.graficaLegend=true;

  	this.infoData=[500, 100, 642]
  	this.infoDataRadar=[{ data:[500, 100, 642], label:"Usuarios"}]

  	this.infoLabels=["Facebook", "Google", "Email"]

  	this.infoChartType="polarArea"
  	//this.infoChartType="pie"
  	//this.infoChartType="doughnut"
  	this.infoChartTypeRadar="radar"

  }

}
