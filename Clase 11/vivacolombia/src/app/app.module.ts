import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MnFullpageModule } from 'ngx-fullpage';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { BannerComponent } from './banner/banner.component';
import { VuelosComponent } from './vuelos/vuelos.component';
import { ContactoComponent } from './contacto/contacto.component';

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    VuelosComponent,
    ContactoComponent
  ],
  imports: [
    BrowserModule,
    MnFullpageModule.forRoot(),
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
