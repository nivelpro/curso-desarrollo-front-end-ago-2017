import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { SliderComponent } from './slider/slider.component';
import { CarrosComponent } from './carros/carros.component';
import { AgmCoreModule } from '@agm/core';
import { MapaComponent } from './mapa/mapa.component';
import { BannerComponent } from './banner/banner.component';
@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
    CarrosComponent,
    MapaComponent,
    BannerComponent
  ],
  imports: [
    AgmCoreModule.forRoot({apiKey:'AIzaSyBJxHMtSqarPP1ZZ8FG7k5RD43eGr0V7bQ'}),
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
