import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.scss']
})
export class MapaComponent implements OnInit {

  latitud:number;
  longitud:number;
  zoom:number;
  stylesMapa:Array<Object>;
  ciudades:Array<Object>;
  puntosActivos:Array<Object>;
  constructor() { }
  ngOnInit() {
  	this.ciudades=[
  		{
  			id:1,
  			nombre:"Medellín",
  			latitud: 6.2277459,
  			longitud:-75.571424,
  			zoom:15,
  			puntos:[
  				{lat:6.2277459,long:-75.571424, titulo:"Andar 30"},
  				{lat:6.2139657,long:-75.5748347, titulo:"Autolarte"},
  				{lat:6.2233515,long:-75.5901984, titulo:"Andar Belen"}
  			]
  		},
  		{
  			id:2,
  			nombre:"Bogotá",
  			latitud: 4.6696265,
  			longitud:-74.05293,
  			zoom:12,
  			puntos:[
  				{lat:4.649196,long:-74.074261, titulo:"Chevroler"},
  				{lat:4.673389,long:-74.088081, titulo:"Auto Niza"},
  				{lat:4.642422,long:-74.062245, titulo:"Celeste"}
  			]
  		},
  		{
  			id:3,
  			nombre:"Cartagena"
  		},
  		{
  			id:4,
  			nombre:"Cali"
  		}];
  	this.puntosActivos = this.ciudades[0]['puntos'];

  	this.latitud = this.ciudades[0]['latitud']
  	this.longitud = this.ciudades[0]['longitud']
  	this.zoom = this.ciudades[0]['zoom']
  	this.stylesMapa = [
		  {
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#212121"
		      }
		    ]
		  },
		  {
		    "elementType": "labels.icon",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#757575"
		      }
		    ]
		  },
		  {
		    "elementType": "labels.text.stroke",
		    "stylers": [
		      {
		        "color": "#212121"
		      }
		    ]
		  },
		  {
		    "featureType": "administrative",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#757575"
		      }
		    ]
		  },
		  {
		    "featureType": "administrative.country",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#9e9e9e"
		      }
		    ]
		  },
		  {
		    "featureType": "administrative.land_parcel",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "featureType": "administrative.locality",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#bdbdbd"
		      }
		    ]
		  },
		  {
		    "featureType": "poi",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#757575"
		      }
		    ]
		  },
		  {
		    "featureType": "poi.park",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#181818"
		      }
		    ]
		  },
		  {
		    "featureType": "poi.park",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#616161"
		      }
		    ]
		  },
		  {
		    "featureType": "poi.park",
		    "elementType": "labels.text.stroke",
		    "stylers": [
		      {
		        "color": "#1b1b1b"
		      }
		    ]
		  },
		  {
		    "featureType": "road",
		    "elementType": "geometry.fill",
		    "stylers": [
		      {
		        "color": "#2c2c2c"
		      }
		    ]
		  },
		  {
		    "featureType": "road",
		    "elementType": "labels",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "featureType": "road",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#8a8a8a"
		      }
		    ]
		  },
		  {
		    "featureType": "road.arterial",
		    "stylers": [
		      {
		        "color": "#afa64b"
		      }
		    ]
		  },
		  {
		    "featureType": "road.arterial",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#13425b"
		      }
		    ]
		  },
		  {
		    "featureType": "road.highway",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#afa64b"
		      }
		    ]
		  },
		  {
		    "featureType": "road.highway.controlled_access",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#4e4e4e"
		      }
		    ]
		  },
		  {
		    "featureType": "road.local",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#616161"
		      }
		    ]
		  },
		  {
		    "featureType": "transit",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#757575"
		      }
		    ]
		  },
		  {
		    "featureType": "water",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#000000"
		      }
		    ]
		  },
		  {
		    "featureType": "water",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#3d3d3d"
		      }
		    ]
		  }
		]
  }

  cambiarCiudad(index){
  	this.puntosActivos = this.ciudades[index]['puntos'];
  	this.latitud = this.ciudades[index]['latitud']
  	this.longitud = this.ciudades[index]['longitud']
  	this.zoom = this.ciudades[index]['zoom']
  }

}
