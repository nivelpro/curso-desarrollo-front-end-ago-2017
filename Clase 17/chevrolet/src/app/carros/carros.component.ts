import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carros',
  templateUrl: './carros.component.html',
  styleUrls: ['./carros.component.scss']
})
export class CarrosComponent implements OnInit {

	categorias:Array<Object>;
	
	categoria1:Array<Object>;
	categoria2:Array<Object>;
	categoria3:Array<Object>;
	categoria4:Array<Object>;
	categoria5:Array<Object>;
	categoria6:Array<Object>;
	categoria7:Array<Object>;

	vercategoria1:Boolean;
	vercategoria2:Boolean;

  constructor() { }

  ngOnInit() {

  	this.vercategoria1=false;
  	this.vercategoria2=true;


  	this.categorias = [
		{
			id:1,
			nombre:"Automoviles"
		},
		{
			id:2,
			nombre:"Camionetas"
		},
		{
			id:3,
			nombre:"Pickups"
		},
		{
			id:4,
			nombre:"Vanes"
		},
		{
			id:5,
			nombre:"Taxis"
		},
		{
			id:6,
			nombre:"Buses"
		},
		{
			id:7,
			nombre:"Camiones"
		}
	];
	this.categoria1 = [
		{
			nombre:"Spark Life",
			imagen:"http://www.chevrolet.com.co/content/dam/Chevrolet/lat-am/Colombia/nscwebsite/es/home/baseball-library/Baseball%20Card%20Library%20for%20Current%20Vehicles/Cars/Spark%20M200/01_Images/140x55_SparkLife.png",
			precio:"$23.000.000"
		},
		{
			nombre:"Spark GT",
			imagen:"http://www.chevrolet.com.co/content/dam/Chevrolet/lat-am/Colombia/nscwebsite/es/home/baseball-library/Baseball%20Card%20Library%20for%20Current%20Vehicles/Cars/spark_gt/01_images/140x55_SparkGT.png",
			precio:"$32.350.000"
		},
		{
			nombre:"Sail",
			imagen:"http://www.chevrolet.com.co/content/dam/Chevrolet/lat-am/Colombia/nscwebsite/es/home/baseball-library/Baseball%20Card%20Library%20for%20Current%20Vehicles/Cars/sail_sedan/01_images/140x55_Sail.png",
			precio:"$33.250.000"
		}
	];
	this.categoria2 = [
		{
			nombre:"Onix Active",
			imagen:"http://www.chevrolet.com.co/content/dam/Chevrolet/lat-am/Colombia/nscwebsite/es/home/baseball-library/Baseball%20Card%20Library%20for%20Current%20Vehicles/Camionetas%20&%20Pickups/Onix_Activ/2017-Onix-Activ-Jellybean_140x55.png",
			precio:""
		},
		{
			nombre:"Tracker",
			imagen:"http://www.chevrolet.com.co/content/dam/Chevrolet/lat-am/Ecuador/nscwebsite/es/Home/Baseball%20Library/SUVs/Tracker/01_images/140x55_Tracker.png",
			precio:"$66.200.000"
		},
		{
			nombre:"Captiva Sport",
			imagen:"http://www.chevrolet.com.co/content/dam/Chevrolet/lat-am/Colombia/nscwebsite/es/home/baseball-library/Baseball%20Card%20Library%20for%20Current%20Vehicles/Camionetas%20&%20Pickups/Captiva/140x55_Captiva.png",
			precio:"$78.050.000"
		}
	]
  }

  verCategoria(id){
  	for(var i=0; i<this.categorias.length;i++){
  		eval("this.vercategoria"+(i+1)+"= true;");	
  	}
  	eval("this.vercategoria"+id+"= false;");
  }


}
