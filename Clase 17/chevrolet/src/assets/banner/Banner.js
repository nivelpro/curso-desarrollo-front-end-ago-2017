(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [
		{name:"Banner_atlas_", frames: [[0,0,800,200],[0,202,354,354],[356,202,192,192]]}
];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.Mapadebits1 = function() {
	this.spriteSheet = ss["Banner_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Mapadebits2 = function() {
	this.spriteSheet = ss["Banner_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Mapadebits3 = function() {
	this.spriteSheet = ss["Banner_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.totenham = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Mapadebits3();
	this.instance.parent = this;
	this.instance.setTransform(-66.7,-66.7,0.695,0.695);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-66.7,-66.7,133.4,133.4);


(lib.texto = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}
	this.frame_59 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(59).call(this.frame_59).wait(1));

	// Capa 1
	this.text = new cjs.Text("17 OCT 1:45PM", "47px 'Verdana'", "#FFFFFF");
	this.text.lineHeight = 59;
	this.text.lineWidth = 395;
	this.text.parent = this;
	this.text.setTransform(-197.6,-28.5);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1).to({y:-21.9},0).wait(1).to({y:-15.2},0).wait(1).to({y:-8.6},0).wait(1).to({y:-1.9},0).wait(1).to({y:4.8},0).wait(1).to({y:11.4},0).wait(1).to({y:18.1},0).wait(1).to({y:24.8},0).wait(1).to({y:31.4},0).wait(1).to({y:38.1},0).wait(1).to({y:44.8},0).wait(1).to({y:51.4},0).wait(1).to({y:58.1},0).wait(1).to({y:64.8},0).wait(1).to({y:71.4},0).wait(1).to({y:78.1},0).wait(1).to({y:84.8},0).wait(1).to({y:91.4},0).wait(1).to({y:98.1},0).wait(1).to({y:104.8},0).wait(1).to({y:111.4},0).wait(1).to({y:118.1},0).wait(38));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-199.6,-30.5,399.3,61.1);


(lib.realmadrid = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Mapadebits2();
	this.instance.parent = this;
	this.instance.setTransform(-54.3,-54.3,0.307,0.307);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-54.3,-54.3,108.7,108.7);


(lib.fondo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Mapadebits1();
	this.instance.parent = this;
	this.instance.setTransform(-400,-100);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-400,-100,800,200);


(lib.totenham_animacion = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_49 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(49).call(this.frame_49).wait(1));

	// Capa 1
	this.instance = new lib.totenham("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(321.3,-16);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({x:314.7},0).wait(1).to({x:308.2},0).wait(1).to({x:301.6},0).wait(1).to({x:295.1},0).wait(1).to({x:288.5},0).wait(1).to({x:282},0).wait(1).to({x:275.4},0).wait(1).to({x:268.8},0).wait(1).to({x:262.3},0).wait(1).to({x:255.7},0).wait(1).to({x:249.2},0).wait(1).to({x:242.6},0).wait(1).to({x:236.1},0).wait(1).to({x:229.5},0).wait(1).to({x:222.9},0).wait(1).to({x:216.4},0).wait(1).to({x:209.8},0).wait(1).to({x:203.3},0).wait(1).to({x:196.7},0).wait(1).to({x:190.2},0).wait(1).to({x:183.6},0).wait(1).to({x:177},0).wait(1).to({x:170.5},0).wait(1).to({x:163.9},0).wait(1).to({x:157.4},0).wait(1).to({x:150.8},0).wait(1).to({x:144.3},0).wait(1).to({x:137.7},0).wait(1).to({x:131.1},0).wait(1).to({x:124.6},0).wait(1).to({x:118},0).wait(1).to({x:111.5},0).wait(1).to({x:104.9},0).wait(1).to({x:98.4},0).wait(1).to({x:91.8},0).wait(1).to({x:85.2},0).wait(1).to({x:78.7},0).wait(1).to({x:72.1},0).wait(1).to({x:65.6},0).wait(1).to({x:59},0).wait(1).to({x:52.5},0).wait(1).to({x:45.9},0).wait(1).to({x:39.3},0).wait(1).to({x:32.8},0).wait(1).to({x:26.2},0).wait(1).to({x:19.7},0).wait(1).to({x:13.1},0).wait(1).to({x:6.6},0).wait(1).to({x:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(254.6,-82.7,133.4,133.4);


(lib.Símbolo1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.fondo("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Símbolo1, new cjs.Rectangle(-400,-100,800,200), null);


(lib.realmadrid_animacion = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_49 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(49).call(this.frame_49).wait(1));

	// Capa 1
	this.instance = new lib.realmadrid("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-242.6,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({x:-237.7},0).wait(1).to({x:-232.8},0).wait(1).to({x:-227.9},0).wait(1).to({x:-223},0).wait(1).to({x:-218},0).wait(1).to({x:-213.1},0).wait(1).to({x:-208.2},0).wait(1).to({x:-203.3},0).wait(1).to({x:-198.3},0).wait(1).to({x:-193.4},0).wait(1).to({x:-188.5},0).wait(1).to({x:-183.6},0).wait(1).to({x:-178.6},0).wait(1).to({x:-173.7},0).wait(1).to({x:-168.8},0).wait(1).to({x:-163.9},0).wait(1).to({x:-158.9},0).wait(1).to({x:-154},0).wait(1).to({x:-149.1},0).wait(1).to({x:-144.2},0).wait(1).to({x:-139.2},0).wait(1).to({x:-134.3},0).wait(1).to({x:-129.4},0).wait(1).to({x:-124.5},0).wait(1).to({x:-119.5},0).wait(1).to({x:-114.6},0).wait(1).to({x:-109.7},0).wait(1).to({x:-104.8},0).wait(1).to({x:-99.8},0).wait(1).to({x:-94.9},0).wait(1).to({x:-90},0).wait(1).to({x:-85.1},0).wait(1).to({x:-80.1},0).wait(1).to({x:-75.2},0).wait(1).to({x:-70.3},0).wait(1).to({x:-65.4},0).wait(1).to({x:-60.4},0).wait(1).to({x:-55.5},0).wait(1).to({x:-50.6},0).wait(1).to({x:-45.7},0).wait(1).to({x:-40.7},0).wait(1).to({x:-35.8},0).wait(1).to({x:-30.9},0).wait(1).to({x:-26},0).wait(1).to({x:-21},0).wait(1).to({x:-16.1},0).wait(1).to({x:-11.2},0).wait(1).to({x:-6.3},0).wait(1).to({x:-1.3},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-297,-54.3,108.7,108.7);


// stage content:
(lib.Banner = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_42 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
		
		/* Evento MouseClick
		Al hacer clic en la instancia del símbolo especificado, se ejecuta una función a la que puede añadir su código personalizado.
		
		Instrucciones:
		1. Añada el código personalizado en una nueva línea después de la línea que dice "// Inicio del código personalizado" más abajo.
		El código se ejecutará al hacer clic en la instancia del símbolo.
		*/
		
		this.movieClip_1.addEventListener("click", fl_MouseClickHandler.bind(this));
		
		function fl_MouseClickHandler()
		{
			// Inicio del código personalizado
			// Este código de ejemplo muestra las palabras "Ratón pulsado" en el panel Salida.
			this.animacion_texto.play();
			this.play();
			// Fin del código personalizado
		}
	}
	this.frame_69 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
		this.animacion_totenham.play();
		this.animacion_realmadrid.play();
		this.animacion_texto.gotoAndStop(0);
		this.play();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(42).call(this.frame_42).wait(27).call(this.frame_69).wait(1));

	// capa_texto
	this.animacion_texto = new lib.texto();
	this.animacion_texto.parent = this;
	this.animacion_texto.setTransform(383,-55.8);

	this.timeline.addTween(cjs.Tween.get(this.animacion_texto).wait(70));

	// capa_logos
	this.animacion_totenham = new lib.totenham_animacion();
	this.animacion_totenham.parent = this;
	this.animacion_totenham.setTransform(632.7,104.7);

	this.animacion_realmadrid = new lib.realmadrid_animacion();
	this.animacion_realmadrid.parent = this;
	this.animacion_realmadrid.setTransform(139.4,92.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.animacion_realmadrid},{t:this.animacion_totenham}]}).wait(70));

	// capa_fondo
	this.instance = new lib.fondo("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(400,100);
	this.instance.alpha = 0;

	this.movieClip_1 = new lib.Símbolo1();
	this.movieClip_1.parent = this;
	this.movieClip_1.setTransform(400,100);
	this.movieClip_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,alpha:1,mode:"independent"},42).wait(28));
	this.timeline.addTween(cjs.Tween.get(this.movieClip_1).to({_off:false},42).to({alpha:0},27).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(242.4,13.6,1178.4,286.4);
// library properties:
lib.properties = {
	width: 800,
	height: 200,
	fps: 24,
	color: "#000000",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/Banner_atlas_.png", id:"Banner_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;